FROM debian:bookworm-slim as vpn

ARG VPN_PORT=${VPN_PORT}

EXPOSE ${VPN_PORT}/udp

RUN apt update
RUN apt install --yes --no-install-recommends \
	iptables \
	iproute2 \
	wireguard-tools

COPY start.sh /usr/local/bin

WORKDIR /root

ENTRYPOINT exec start.sh
